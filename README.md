# Front-End-Challenge-01

### Descrição

- Desafio técnico para oportunidade de Pessoa Front-End (HTML / CSS / JS) no Grupo MContigo.

### Etapas
1. Criar um **fork** e entregar o link do repositório em seu usuário. Vamos avaliar os commits em seu próprio repositório pessoal.
2. Usar o layout da home do projeto Unycos disponível no Figma: https://www.figma.com/file/FdDMsfrxDEkVdjHvnPBQnR/Untitled?node-id=0%3A1;
2. Recortar usando HTML 5 e CSS3 com os efeitos e animações propostas versão Desktop e Responsiva Mobile;
3. Usar boas práticas de HTML para SEO;
4. Usar estratégias de CSS Module;


### Boas Práticas
1. HTML otimizado para SEO;
2. Idioma do código e comentátios: inglês;
3. Código legível e bem estruturado;
3. Comentários objetivos;
4. .gitignore configurado;
5. Versão desktop e responsiva mobile cross-browser;

### Plus
1. Componetizar o projeto em React.js;

### Prazo
1. Cinco dias (5 dias) úteis a partir do e-mail de envio do Desafio Tech. Data exata está no email.
2. Notificar no e-mail a finalização e enviar somente a url do repositório para avaliação;
3. Sempre usar no e-mail a opção "Responder a Todos";

### Dúvidas
- gferreira@mcontigo.com e bruno@mcontigo.com

